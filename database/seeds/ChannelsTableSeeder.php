<?php

use App\Channel;
use Illuminate\Database\Seeder;

class ChannelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
            $channel_one =  ['title' => 'Laravel', 'slug' => str_slug('Laravel')];
            $channel_two = ['title' => 'Django', 'slug' => str_slug('Django')];
            $channel_three = ['title' => 'Nodejs', 'slug' => str_slug('Nodejs')];
            $channel_four = ['title' => 'Python', 'slug' => str_slug('Python')];
            $channel_five = ['title' => 'PHP', 'slug' => str_slug('PHP')];
            $channel_six = ['title' => 'Lumen', 'slug' => str_slug('Lumen')];
            $channel_seven = ['title' => 'CSS3', 'slug' => str_slug('CSS3')];
            $channel_eight = ['title' => 'Forge', 'slug' => str_slug('Forge')];

        Channel::create($channel_one);
        Channel::create($channel_two);
        Channel::create($channel_three);
        Channel::create($channel_four);
        Channel::create($channel_five);
        Channel::create($channel_six);
        Channel::create($channel_seven);
        Channel::create($channel_eight);
}
}
