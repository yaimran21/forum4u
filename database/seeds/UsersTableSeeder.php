<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::create([
            'name' => 'Admin',
            'password' => bcrypt('admin'),
            'email' => 'admin@forum4u.com',
            'admin' => 1,
            'avatar' => asset('avatars/avatar.png')
        ]);
        App\User::create([
            'name' => 'Imran',
            'password' => bcrypt('imran'),
            'email' => 'imran@forum4u.com',
            'admin' => 0,
            'avatar' => asset('avatars/avatar.png')
        ]);
    }
}
