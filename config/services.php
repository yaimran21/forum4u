<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

//    'mailgun' => [
//        'domain' => env('MAILGUN_DOMAIN'),
//        'secret' => env('MAILGUN_SECRET'),
//        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
//    ],
//
//    'postmark' => [
//        'token' => env('POSTMARK_TOKEN'),
//    ],
//
//    'ses' => [
//        'key' => env('AWS_ACCESS_KEY_ID'),
//        'secret' => env('AWS_SECRET_ACCESS_KEY'),
//        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
//    ],
    'github' => [
        'client_id' => 'eaf0785eb2a5f9a429ca',
        'client_secret' => '483fa4515c8d29ce698986598da68f8eda4c0454',
        'redirect' => 'http://127.0.0.1:8000/github/redirect',
    ],
    'facebook' => [
        'client_id' => '823523398092740',
        'client_secret' => 'c9eedd4f1efc882ae2852f86e66d4cdf',
        'redirect' => 'http://127.0.0.1:8000/github/redirect',
    ],
];
