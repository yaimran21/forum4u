<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/discussion', function () {
    return view('discussion.index');
});

Auth::routes();

Route::get('/forum', 'ForumController@index')->name('forum');
Route::get('channel/{id}', 'ForumController@channel')->name('channel');

// Custom route

Route::get('{provider}/auth', [
    'uses' => 'SocialController@auth',
    'as' => 'social.auth'
]);

Route::get('{provider}/redirect', [
    'uses' => 'SocialController@auth_callback',
    'as' => 'social.auth_callback'
]);

Route::get('discussion/{id}/{slug}', [
    'uses' =>'DiscussionController@show',
    'as' => 'discussion.show'
]);
Route::group(['middleware' => 'auth'], function () {
    Route::resource('channel', 'ChannelController');

    Route::get('discussion/create', [
        'uses' =>'DiscussionController@create',
        'as' => 'discussion.create'
    ]);
    Route::get('discussion/update/{slug}/{id}', [
        'uses' =>'DiscussionController@edit',
        'as' => 'discussion.edit'
    ]);
    Route::post('discussion/update/{id}', [
        'uses' =>'DiscussionController@update',
        'as' => 'discussion.update'
    ]);
    Route::post('discussion/store', [
        'uses' =>'DiscussionController@store',
        'as' => 'discussion.store'
    ]);

    Route::post('discussion/reply/{id}', [
        'uses' =>'DiscussionController@reply',
        'as' => 'discussion.reply'
    ]);
    Route::get('reply/like/{id}', [
        'uses' =>'ReplyController@like',
        'as' => 'reply.like'
    ]);
    Route::get('reply/unlike/{id}', [
        'uses' =>'ReplyController@unlike',
        'as' => 'reply.unlike'
    ]);
    Route::get('reply/edit/{id}', [
        'uses' =>'ReplyController@edit',
        'as' => 'reply.edit'
    ]);
    Route::post('reply/update/{id}', [
        'uses' =>'ReplyController@update',
        'as' => 'reply.update'
    ]);

    Route::get('discussion/best/reply/{id}', [
        'uses' =>'ReplyController@best_answer',
        'as' => 'discussion.best_answer'
    ]);

    // Start watch route

    Route::get('watch/{id}', [
        'uses' =>'WatchController@watch',
        'as' => 'discussion.watch'
    ]);

    Route::get('unwatch/{id}', [
        'uses' =>'WatchController@unwatch',
        'as' => 'discussion.unwatch'
    ]);


});


