<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Reply extends Model
{
    protected $fillable = ['content', 'user_id', 'discussion_id'];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function discussion(){
        return $this->belongsTo('App\Discussion');
    }

    public function likes(){
        return $this->hasMany('App\Like');
    }

    public function is_liked_by_auth_user(){
        $user = Auth::id();
        $likers = array();
        foreach($this->likes()->get() as $like){
            array_push($likers, $like->user_id);
        }

        if (in_array($user, $likers)){
            return true;
        }
        else{
            return false;
        }
    }


}
