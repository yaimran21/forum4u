<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Discussion extends Model
{
    protected $fillable= ['title', 'slug','content', 'user_id', 'channel_id'];

    public function channel(){
        return $this->belongsTo(Channel::class);
    }
    public function user(){
        return $this->belongsTo(User::class);
    }

    public function replies(){
        return $this->hasMany(Reply::class);
    }
    public function watches(){
        return $this->hasMany(Watch::class);
    }

    public function is_being_watched_by_auth_user(){
        $user = Auth::id();
        $watchers = array();

        foreach ($this->watches()->get() as $watch){
             array_push($watchers, $watch->user_id);
        }
        if (in_array($user, $watchers)){
            return true;
        }

        else{
            return false;
        }
    }

    public function has_best_answer(){
        $best_answer = false;
        foreach ($this->replies()->get() as $reply){
            if ($reply->best_answer){
                $best_answer = True;
                break;
            }
        }
        return $best_answer;
    }

}
