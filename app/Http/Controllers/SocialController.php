<?php

namespace App\Http\Controllers;

use \App\User;
use Laravel\Socialite\Facades\Socialite;


class SocialController extends Controller
{

    public function auth($provider)
    {

        return Socialite::driver($provider)->redirect();
    }


    public function auth_callback($provider)
    {
        $get_info = Socialite::driver($provider)->user();
        $user = $this->createUser($get_info, $provider);
        auth()->login($user);
        return redirect('/forum');
    }

    function createUser($getInfo,$provider){

        $user = User::where('email', $getInfo->email)->first();
        if (!$user) {
            $user = User::create([
                'name'     => $getInfo->name,
                'email'    => $getInfo->email,
                'avatar' => $getInfo->avatar,
                'provider' => $provider,
            ]);
        }
        return $user;
    }

}
