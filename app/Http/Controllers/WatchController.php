<?php

namespace App\Http\Controllers;
use App\Discussion;
use Auth;
use App\Watch;
use Session;
use Illuminate\Http\Request;

class WatchController extends Controller
{
    public function watch($id){

//        $discussion = Discussion::find($id);

        Watch::create([
           'discussion_id' => $id,
           'user_id' => Auth::id()
        ]);
        Session::flash('success', 'You are watching this discussion');
        return redirect()->back();
    }

    public function unwatch($id){

        $watch = Watch::where('discussion_id', $id)->where('user_id', Auth::id());
        $watch->delete();
        Session::flash('success', 'You are no longer watching this discussion');
        return redirect()->back();
    }

}
