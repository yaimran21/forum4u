<?php

namespace App\Http\Controllers;

use App\User;
use App\Discussion;
use App\Notifications\NewReplyAdded;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Reply;
use Notification;
use Session;


class DiscussionController extends Controller
{
    public function create(){
        return view('discussion.create');
    }
    public function edit($slug, $id){
        $discussion = Discussion::find($id);
        return view('discussion.edit')->with('discussion', $discussion);
    }
    public function store(Request $request){
        $this->validate($request,[
            'title' => 'required',
            'channel' => 'required',
            'discussion' => 'required'
        ]);

        $discussion = Discussion::create([
            'title' => $request->title,
            'slug' => str_slug($request->title),
            'content' => $request->discussion,
            'channel_id' => $request->channel,
            'user_id' => Auth::id()

        ]);
        Session::flash('success', 'Discussion Created Successfully');
        return redirect()->route('discussion.show', ['id'=> $discussion->id,'slug'=> $discussion->slug]);
    }
    public function Update(Request $request, $id){
        $this->validate($request,[
            'discussion' => 'required'
        ]);

        $discussion = Discussion::find($id);
        $discussion->content = $request->discussion;
        $discussion->save();
        Session::flash('success', 'Discussion Update Successfully');
        return redirect()->route('discussion.show', ['id'=> $discussion->id,'slug'=> $discussion->slug]);
    }

    public function show($id, $slug){
        $discussion = Discussion::find($id);
            $best_answer = $discussion->replies()->where('best_answer', 1)->first();
        return view('discussion.show')->with('discussion', $discussion)->with('best_answer', $best_answer);
    }

    public function reply(Request $request,$id){

//        $this->validate($request,[
//            'user_id' => 'required',
//            'discussion_id' => 'required',
//            'content' => 'required'
//        ]);

        $discussion = Discussion::find($id);


        $reply = Reply::create([
            'user_id' => Auth::id(),
            'discussion_id' => $discussion->id,
            'content' => $request->reply
        ]);

        $reply->user->points += 25;
        $reply->user->save();

        $watchers = array();

        foreach ($discussion->watches()->get() as $watch){
            array_push($watchers, $watch->user_id);
        }
//        $email = 'admin@forum4u.net';
        $watchers = User::whereIn('id', $watchers)->get();

//        dd($watchers);

        Notification::send($watchers, New NewReplyAdded($discussion));


        Session::flash('success', 'Replied to the discussion');
        return redirect()->back();
    }
}
