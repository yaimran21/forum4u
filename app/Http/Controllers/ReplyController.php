<?php

namespace App\Http\Controllers;

use App\Like;
use App\Reply;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class ReplyController extends Controller
{

    public function edit(Request $request, $id){
        $reply = Reply::find($id);
        return view('reply.edit', ['reply'=> $reply]);
    }
    public function update(Request $request, $id){
        $this->validate($request, [
            'reply' => 'required'
        ]);

        $reply = Reply::find($id);
        $reply->content = $request->reply;
        $reply->save();

        return redirect()->route('discussion.show', ['id'=> $reply->discussion_id, 'slug'=> $reply->discussion->slug]);
    }

    public function like(Request $request, $id){
        $reply = Reply::find($id);
        Like::create([
            'reply_id' => $reply->id,
            'user_id' => Auth::id(),
        ]);
        Session::flash('success', 'Liked successfully');
        return redirect()->back();

    }

    public function unlike($id){
        $like = Like::where('reply_id', $id)->where('user_id', Auth::id());
        $like->delete();
        Session::flash('danger', 'Unlike successfully');
        return redirect()->back();
    }

    public function best_answer($id){
        $best_ans = Reply::find($id);
        $best_ans->best_answer = 1;
        $best_ans->save();

        $best_ans->user->points += 100;
        $best_ans->user->save();

        Session::flash('Reply has been marked as best answer');
        return redirect()->back();
    }
}
