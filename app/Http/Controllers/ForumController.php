<?php

namespace App\Http\Controllers;
use Auth;
use App\Channel;
use App\Discussion;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;


class ForumController extends Controller
{
    public function index(){

        switch (request('filter')) {
            case 'me':
                $discussions = Discussion::where('user_id', Auth::id())->orderBy('created_at', 'desc')->paginate(3);
                break;
            case 'solved':
                $solved_answer_discussions = array();
                foreach (Discussion::orderBy('created_at', 'desc')->get() as $d){
                    if ($d->has_best_answer()){
                        array_push($solved_answer_discussions, $d);
                    }
                }
                $discussions = new Paginator($solved_answer_discussions,3);
                break;
            case 'unsolved':
                $unsolved_discussions = array();
                foreach (Discussion::orderBy('created_at', 'desc')->get() as $d){
                    if (!$d->has_best_answer()){
                        array_push($unsolved_discussions, $d);
                    }
                }
                $discussions = new Paginator($unsolved_discussions,3);

                // Custom Paginator
//                $currentPage = LengthAwarePaginator::resolveCurrentPage();
//                $perPage = 3;
//
//                $unsolved_discussions = array_reverse(array_sort($unsolved_discussions , function ($value) {
//                    return $value['created_at'];
//                }));
//                $currentItems = array_slice($unsolved_discussions, $perPage * ($currentPage - 1), $perPage);
//
//                $paginator = new LengthAwarePaginator($currentItems, count($unsolved_discussions), $perPage, $currentPage);
//                $discussions = $paginator->appends('forum/filter', request('filter'));
                break;
            default:
                $discussions = Discussion::orderBy('created_at', 'desc')->paginate(3);
        }
        return view('forum', ['discussions' => $discussions]);
    }

    public function channel($id){
        $discussions = Channel::find($id)->discussions()->paginate(5);
        return view('channel')->with('discussions', $discussions);
    }
}
