@extends('layouts.app')

@section('content')
    <div class="card">
        <h5 class="card-header text-center">Update a Reply</h5>

        <div class="card-body">
            <form action="{{route('reply.update', ['id'=> $reply->id])}}" method="post">
                {{csrf_field()}}
                <div class="form-group">
                    <textarea type="text" name="reply" class="form-control" cols="30" rows="10">{{ $reply->content }}</textarea>
                    <br>
                    <button class="form-control btn btn-success" type="submit">
                        Save Reply changes
                    </button>
                </div>

            </form>
        </div>
    </div>
@endsection
