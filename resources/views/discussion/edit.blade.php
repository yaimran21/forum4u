@extends('layouts.app')

@section('content')
    <div class="card">
        <h5 class="card-header text-center">Update Discussion</h5>

        <div class="card-body">
            <form action="{{route('discussion.update', ['id'=> $discussion->id])}}" method="post">
                {{csrf_field()}}
                <div class="form-group">
                    <textarea type="text" id="editor" name="discussion" class="form-control" cols="30" rows="10">{{ $discussion->content }}</textarea>
                    <br>
                    <button class="form-control btn btn-success" type="submit">
                        Save discussion changes
                    </button>
                </div>

            </form>
        </div>
    </div>
@endsection
