@extends('layouts.app')

@section('content')
            <div class="card">
                <h5 class="card-header text-center">Create a New Discussion</h5>

                <div class="card-body">
                    <form action="{{route('discussion.store')}}" method="post">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" name="title" value="{{old('title')}}" class="form-control">
                            <label for="channel">Pick a Channel</label>
                            <select class="form-control" name="channel" id="channel">
                                <option value="">--Select a channel--</option>
                            @foreach($channels as $channel)
                                    <option value="{{$channel->id}}">{{$channel->title}}</option>
                                    @endforeach
                            </select>
                            <label for="discussion">Ask a Question</label>
                            <textarea type="text" id="editor" name="discussion" class="form-control" cols="30" rows="10">{{old('content')}}</textarea>
                            <br>
                            <button class="form-control btn btn-success" type="submit">
                                Save Discussion
                            </button>
                        </div>

                    </form>
                </div>
            </div>
@endsection
