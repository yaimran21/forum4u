@extends('layouts.app')
@section('title', $discussion->title)
@section('content')
    <div class="card">
        <div class="card-header">
            <img class="rounded-circle" src="{{ asset($discussion->user->avatar) }}" alt="Logo" width="40px" height="40px">
            <span class="text-muted"> Discussed by - {{$discussion->user->name}} ({{$discussion->user->points}}) || <b>{{$discussion->created_at->diffForHumans()}}</b></span>
            @if(Auth::id() == $discussion->user_id and !$discussion->has_best_answer())
                <a class=" float-right btn btn-outline-info btn-sm text-decoration-none ml-1"
                   href="{{route('discussion.edit', ['slug'=>$discussion->slug, 'id' => $discussion->id])}}">Edit</a>
            @endif
            @if($discussion->is_being_watched_by_auth_user())
                <a class=" float-right btn btn-outline-danger btn-sm text-decoration-none ml-1"
                   href="{{route('discussion.unwatch', ['id' => $discussion->id])}}">Unwatch</a>
            @else
                <a class=" float-right btn btn-outline-success btn-sm text-decoration-none ml-1"
                   href="{{route('discussion.watch', ['id' => $discussion->id])}}">Watch</a>
            @endif
            @if($discussion->has_best_answer())
                <span class="btn btn-success btn-sm float-right">Open</span>

            @else
                <span class="btn btn-danger btn-sm float-right ">Close</span>

            @endif
        </div>
        <div class="card-body">
            <h4> {{$discussion->title}}</h4>
            {!! Markdown::convertToHtml($discussion->content) !!}
            @if($best_answer)
                <div class="text-center card border-success ">
                    <div class=" card-body">
                        <h3 class="text-center">Best Answer</h3>
                        <img class="rounded-circle" src="{{ asset($discussion->user->avatar) }}" alt="Logo" width="70px" height="70px">
                        <span class=""> Answered  by - {{$best_answer->user->name}} ({{$best_answer->user->points}})</span>
                        <hr>
                        <p class="text-justify border-success"> {!! Markdown::convertToHtml($best_answer->content) !!} </p>
                    </div>
                </div>
            @endif
        </div>
        <div class="card-footer">
            <span>{{$discussion->replies->count()}} replies</span>
            <a class=" float-right btn btn-outline-primary btn-sm text-decoration-none"
               href="{{route('channel', ['id'=>$discussion->channel->id, 'slug'=>$discussion->channel->slug])}}">{{$discussion->channel->title}}</a>
        </div>
    </div>
    <br>
    @foreach($discussion->replies as $r)
        <div class="card">
            <div class="card-header">
                <img class="rounded-circle" src="{{ asset($r->user->avatar) }}" alt="Logo" width="40px" height="40px">
                <span class="text-muted"> Replied by - {{$r->user->name}} || <b>{{$r->created_at->diffForHumans()}} (reply)</b></span>
                @if(!$best_answer)
                    @if(Auth::id()== $discussion->user->id)
                        <a href="{{route('discussion.best_answer', ['id'=> $r->id])}}" class="btn btn-sm btn-info float-right">Mark as best answer</a>
                    @endif
                    @if(Auth::id()== $r->user->id)
                        <a href="{{route('reply.edit', ['id'=> $r->id])}}" class="btn btn-sm btn-info float-right mr-1">Edit</a>
                    @endif
                @endif

            </div>
            <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                    {!! Markdown::convertToHtml($r->content ) !!}
            </div>
            <div class="card-footer text-muted">
                @if($r->is_liked_by_auth_user())
                    <a href="{{route('reply.unlike', ['id' => $r->id])}}" class="btn btn-danger btn-sm">Unlike <span class="badge badge-pill badge-dark">{{$r->likes->count()}}</span></a>
                @else
                    <a href="{{route('reply.like', ['id' => $r->id])}}" class="btn btn-success btn-sm">Like <span class="badge badge-pill badge-dark">{{$r->likes->count()}}</span></a>
                @endif
            </div>
        </div>
        <br>
    @endforeach

    <div class="card">
        <div class="card-body">
            @if(Auth::check())
                <div class="form-group">
                    <form action="{{route('discussion.reply', ['id'=> $discussion->id])}}" method="post">
                        {{csrf_field()}}
                        <label for="reply">Leave a reply</label>
                        <textarea class="form-control" name="reply" id="editor" cols="30" rows="10"></textarea>
                        <br>
                        <button class=" btn btn-xs btn-success " type="submit">
                            Reply
                        </button>
                    </form>
                </div>
            @else
                <div class="text-center">
                    <h2>Sign in to leave a reply</h2>
                </div>
            @endif

        </div>
    </div>
    {{--    </div>--}}
@endsection
