
<!doctype html>
<html lang="en">
<head>
    @include('_partial.head')
</head>
<body>
<div id="app">
    @include('_partial.nav')

    <div class="container">
        @if($errors->any())
            <ul class="list-group-item">
                @foreach($errors->all() as $error)
                    <li class="list-group-item text-danger">
                        {{$error}}
                    </li>
                @endforeach
            </ul>
        @endif
    </div>
    <br>
    <br>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                @include('_partial.channel')
            </div>
            <div class="col-md-8">
                @yield('content')
            </div>
        </div>
    </div>
</div>
@include('_partial.footer')
<script>
    @if(Session::has('success'))
    toastr.success('{{Session::get('success')}}');
    @endif
</script>
<script>hljs.initHighlightingOnLoad();</script>
<script>
    ClassicEditor
        .create( document.querySelector( '#editor' ) )
        .catch( error => {
            console.error( error );
        } );
</script>

</body>
</html>
