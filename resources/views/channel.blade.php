@extends('layouts.app')
@section('title', 'discussion')
@section('content')

    @foreach($discussions as $d)
        <div class="card">
            <div class="card-header">
                <img class="rounded-circle" src="{{ asset('avatars/avatar.png') }}" alt="" width="40px" height="40px">
                <span class="text-muted"> Discussed by - {{$d->user->name}} || <b>{{$d->created_at->diffForHumans()}}</b></span>
                @if($d->is_being_watched_by_auth_user())
                    <a class=" float-right btn btn-outline-danger btn-sm text-decoration-none ml-1"
                       href="{{route('discussion.unwatch', ['id' => $d->id])}}">Unwatch</a>
                @else
                    <a class=" float-right btn btn-outline-success btn-sm text-decoration-none ml-1"
                       href="{{route('discussion.watch', ['id' => $d->id])}}">Watch</a>
                @endif
                @if($d->has_best_answer())
                    <span class="btn btn-success btn-sm float-right">Open</span>

                @else
                    <span class="btn btn-danger btn-sm float-right ">Close</span>

                @endif
            </div>
            <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                <h4> {{$d->title}}</h4>
                {{ str_limit($d->content, $limit = 200, $end = '...') }}
                <a href="{{route('discussion.show', ['id'=>$d->id,'slug' => $d->slug])}}">read more</a>
            </div>
            <div class="card-footer">
                <span>{{$d->replies->count()}} replies</span>
                <a class=" float-right btn btn-outline-primary btn-sm text-decoration-none"
                   href="{{route('channel', ['id'=>$d->channel->id, 'slug'=>$d->channel->slug])}}">{{$d->channel->title}}</a>

            </div>
        </div>
        <br>
    @endforeach
    <div class="text-center">
        {{$discussions->links()}}
    </div>
@endsection
