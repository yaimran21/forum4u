<a href="{{route('discussion.create')}}" class=" form-control btn btn-primary">Create Discussion</a>
<br>
<br>
<div class="card">
    <div class="card-body">
        <ul class="list-group">
            <li class="list-group-item">
                <a href="/forum" style="text-decoration:none">Home</a>
            </li>
            <li class="list-group-item">
                <a href="/forum?filter=me" style="text-decoration:none">My Discussion</a>
            </li>
            <li class="list-group-item">
                <a href="/forum?filter=solved" style="text-decoration:none">Solved Discussion</a>
            </li>
            <li class="list-group-item">
                <a href="/forum?filter=unsolved" style="text-decoration:none">Unsolved Discussion</a>
            </li>
        </ul>
    </div>
    @if(Auth::check())
        @if(Auth::user()->admin)
            <div class="card-body">
                <ul class="list-group">
                    <li class="list-group-item">
                        <a href="{{route('channel.index')}} " style="text-decoration:none">All Channels</a>
                    </li>
                </ul>
            </div>
            @endif
    @endif
</div>

<br>
<div class="card">
    <div class="card-header">
        Channels
    </div>

    <div class="card-body">
        <ul class="list-group">
            @foreach($channels as $channel)
                <li class="list-group-item">
                    <a class="text-decoration-none" href="{{route('channel', ['id'=>$channel->id])}}">{{$channel->title}}</a>
                </li>
            @endforeach
        </ul>
    </div>
</div>

