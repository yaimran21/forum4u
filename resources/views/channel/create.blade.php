@extends('layouts.app')

@section('content')
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form action="{{route('channel.store')}}" method="post">
                            {{csrf_field()}}
                            <div class="form-group">
                                <input type="input" name="channel" class="form-control">
                            </div>
                            <div class="form-group">
                                <button class="btn btn-success text-center" type="submit">
                                    Save Channel
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
@endsection
