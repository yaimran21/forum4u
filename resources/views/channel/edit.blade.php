@extends('layouts.app')

@section('content')

                <div class="card">
                    <div class="card-header">Edit Channel: {{$channel->title}}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form action="{{route('channel.update', ['channel'=> $channel->id])}}" method="post">
                            {{csrf_field()}}
                            {{method_field('put')}}
                            <div class="form-group">
                                <input type="input" name="channel" value="{{$channel->title}}" class="form-control">
                            </div>
                            <div class="form-group">
                                <button class="btn btn-success text-center" type="submit">
                                    Save Update
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
@endsection
